Version 2/181103 of Vorple ink by Nathanael Marion begins here.

"Allows Inform to use a story written with ink. Requires Vorple and the JavaScript library inkjs."

Include version 3 of Vorple by Juhana Leinonen.

Part 1 - The main loop

The ink initialisation command is initially "".

First when play begins (this is the initialise ink story rule):
	block the user interface;
	execute JavaScript command "$.getJSON('story.json', function(storyContent) { story = new inkjs.Story(storyContent); vorple.layout.unblock(); [if the ink initialisation command is not empty]; vorple.prompt.queueCommand('[escaped ink initialisation command]', true)[end if]})".

To decide whether ink can continue:
	execute JavaScript command "return story.canContinue";
	decide on whether or not the JavaScript command returned true.

To decide what text is the next ink line:
	execute Javascript command "return story.Continue()";
	decide on the text returned by the JavaScript command.

To decide what text is the next ink lines:
	execute JavaScript command "return story.ContinueMaximally()";
	decide on the text returned by the JavaScript command.

To decide what number is the current number of ink choices:
	execute JavaScript command "return story.currentChoices.length";
	decide on the number returned by the JavaScript command.

To decide what list of texts is the current ink choices:
	let L be a list of texts;
	let N be the current number of ink choices;
	if N > 0:
		repeat with X running from 0 to (N - 1):
			execute JavaScript command "return story.currentChoices[bracket][X][close bracket].text";
			add the text returned by the JavaScript command to L;
	decide on L.

To select ink choice number (N - a number):
	execute JavaScript command "story.ChooseChoiceIndex([N - 1])".

Part 2 - Saving and loading

[TODO: This part hasn't been tested.]

To decide what text is the ink story state:
	execute JavaScript command "return story.state.toJson()";
	decide on the text returned by the JavaScript command.

To load the ink story state (T - a text):
	execute JavaScript command "story.state.LoadJson('[escaped T]')".

Chapter 1 - Saving and loading the story state automatically

The _inksave is a text that varies.

[This will save the ink story state when the player attempts to save the game. The only drawback is that the story state will be stored even if the save failed.]
Check saving the game (this is the store the ink story state before saving rule):
	now _inksave is the ink story state.

[Loads the ink story state after restoring a game. I use the response system because I don't know another way to do something after restoring.]
After issuing the response text of the restore the game rule response (B) (this is the load the ink story state after after restoring rule):
	load the ink story state _inksave.


Part 3 - Tags

Chapter 1 - Line by line tags

To decide what number is the current number of ink tags:
	execute JavaScript command "return story.currentTags.length";
	decide on the number returned by the JavaScript command.

To decide what list of texts is the current ink tags:
	let L be a list of texts;
	let N be the current number of ink tags;
	if N > 0:
		repeat with X running from 0 to (N - 1):
			execute JavaScript command "return story.currentTags[bracket][X][close bracket]";
			add the text returned by the JavaScript command to L;
	decide on L.

Chapter 2 - Knot tags

To decide what number is the number of ink tags in (K - a text):
	execute JavaScript command "return story.TagsForContentAtPath('[K]').length";
	decide on the number returned by the JavaScript command.

To decide what list of texts is the ink tags of (K - a text):
	let L be a list of texts;
	let N be the number of ink tags in K;
	if N > 0:
		repeat with X running from 0 to (N - 1):
			execute JavaScript command "return story.TagsForContentAtPath('[K]')[bracket][X][close bracket]";
			add the text returned by the JavaScript command to L;
	decide on L.

Chapter 3 - Global tags

To decide what number is the number of global ink tags:
	execute JavaScript command "return story.globalTags.length";
	decide on the number returned by the JavaScript command.

To decide what list of texts is the global ink tags:
	let L be a list of texts;
	let N be the number of global ink tags;
	if N > 0:
		repeat with X running from 0 to (N - 1):
			execute JavaScript command "return story.globalTags[bracket][X][close bracket]";
			add the text returned by the JavaScript command to L;
	decide on L.

Part 4 - Jumping to a particular knot

To jump to (T - a text) in ink:
	execute JavaScript command "story.ChoosePathString('[T]')";

Part 5 - Setting and getting ink variable

Chapter 1 - Setting

To set the ink variable (var - a text) to (val - a text):
	execute JavaScript command "story.variablesState[bracket]'[var]'[close bracket] = '[escaped val]'".

To set the ink variable (var - a text) to (val - a number):
	execute JavaScript command "story.variablesState[bracket]'[var]'[close bracket] = [val]".

To set the ink variable (var - a text) to (val - a truth state):
	execute JavaScript command "story.variablesState[bracket]'[var]'[close bracket] = [val]".

Chapter 2 - Getting

To decide what text is the value stored in the/-- ink variable (var - a text):
	execute JavaScript command "return story.variablesState[bracket]'[var]'[close bracket]";
	decide on the value returned by the JavaScript command.

To decide what text is the text stored in the/-- ink variable (var - a text):
	execute JavaScript command "return story.variablesState[bracket]'[var]'[close bracket]";
	decide on the text returned by the JavaScript command.

To decide what number is the number stored in the/-- ink variable (var - a text):
	execute JavaScript command "return story.variablesState[bracket]'[var]'[close bracket]";
	decide on the number returned by the JavaScript command.

To decide whether the ink variable (var - a text) is true:
	execute JavaScript command "return story.variablesState[bracket]'[var]'[close bracket]";
	decide on whether or not the number returned by the JavaScript command is 1.

Part 6 - Visit counts

To decide what number is the ink visit count of (T - a text):
	execute JavaScript command "return story.state.VisitCountAtPathString('[T]')";
	decide on the number returned by the JavaScript command.

Part 7 - Variable observers

[TODO: Seems difficult to integrate to Inform. However, workarounds are easy: it's possible to watch a variable in a every turn rule and update the status line, for example.]

Part 8 - External functions

[TODO: Seems impossible to integrate to Inform: while it would be easy to bind a JavaScript function to an ink function, I doubt it is possible to bind a Inform rule to an ink function. If the author needs this feature, they will have to do it in JavaScript.]

Part 9 - Evaluating ink functions

To evaluate the ink function (fct - a text) with (args - a list of texts):
	let array be "[args in brace notation]";
	replace character number 1 in array with "[bracket]";
	replace character number (number of characters in array) in array with "[close bracket]";
	execute JavaScript command "story.EvaluateFunction('[fct]', [array])".

To decide what text is the value returned by the/-- ink function (fct - a text) with (args - a list of texts):
	let array be "[args in brace notation]";
	replace character number 1 in array with "[bracket]";
	replace character number (number of characters in array) in array with "[close bracket]";
	execute JavaScript command "return story.EvaluateFunction('[fct]', [args])";
	decide on the value returned by the JavaScript command.

To decide what text is the text returned by the/-- ink function (fct - a text) with (args - a list of texts):
	let array be "[args in brace notation]";
	replace character number 1 in array with "[bracket]";
	replace character number (number of characters in array) in array with "[close bracket]";
	execute JavaScript command "return story.EvaluateFunction('[fct]', [array])";
	decide on the text returned by the JavaScript command.

To decide what number is the number returned by the/-- ink function (fct - a text) with (args - a list of texts):
	let array be "[args in brace notation]";
	replace character number 1 in array with "[bracket]";
	replace character number (number of characters in array) in array with "[close bracket]";
	execute JavaScript command "return story.EvaluateFunction('[fct]', [array])";
	decide on the number returned by the JavaScript command.

To decide whether the ink function (fct - a text) with (args - a list of texts) returned (bool - a truth state):
	let array be "[args in brace notation]";
	replace character number 1 in array with "[bracket]";
	replace character number (number of characters in array) in array with "[close bracket]";
	execute JavaScript command "return story.EvaluateFunction('[fct]', [array])";
	decide on whether or not the JavaScript command returned bool.

[TODO: Right now, you have to call the function twice if you want to get the value it returned (with one of the phrases above) and the texte it output (with the phrase below). Find a way so that's not the case.]
To decide what text is the output of the/-- ink function (fct - a text) with (args - a list of texts):
	let array be "[args in brace notation]";
	replace character number 1 in array with "[bracket]";
	replace character number (number of characters in array) in array with "[close bracket]";
	execute JavaScript command "return story.EvaluateFunction('[fct]', [array], true).output";
	decide on the text returned by the JavaScript command.

Vorple ink ends here.

---- DOCUMENTATION ----

This extension allows us to use the scripting language ink by inkle (http://www.inklestudios.com) with Vorple, thanks to the ink JavaScript implementation inkjs (https://github.com/y-lohse/inkjs). We'll assume you are already familiar with ink.

Part: Getting started

Chapter: Why use ink with Inform?

Why use an interactive fiction authoring system inside another interactive fiction authoring system? They can be many reasons. We could use ink to write a choice-based IF while making use of the Inform world model. We could use ink to write the dialogs in our parser IF. Or maybe we just want to use ink whithout diving into JavaScript or Unity, and just use Inform. Whatever are the reasons, the following will explain of to use ink with Inform 7.

Chapter: Ink documentation

A tutorial for writing ink is available in the ink repository:

	https://github.com/inkle/ink/blob/master/Documentation/WritingWithInk.md

A complete description of the ink functions (in C#) is also available:

	https://github.com/inkle/ink/blob/master/Documentation/RunningYourInk.md

The rest of this documentation will go through all of them, but feel free to look at the previous link for more informations.

Chapter: Installation

Before using this extension, we'll need to download the file "ink.js" from

	https://github.com/y-lohse/inkjs/releases/latest

and place it in the materials folder. We also need to place the file "story.json" containing our ink story in the same folder. All that is left to do is to indicate to Inform to bundle these files with our game:

	Include Vorple ink by Nathanael Marion.
	Release along with the "Vorple" interpreter.
	Release along with JavaScript "ink.js".
	Release along with the file "story.json".

Chapter: The main loop

An ink story is usually displayed line by line until no content is available, then the list of choices is shown. To know whether there are more lines to be displayed, we can use the following phrase:

	if ink can continue:

And to get the next line of the ink story:

	say "[the next ink line]";

If the story can continue, a new line will be said each time. We can also get the entire next content:

	say "[the next ink lines]";

When the story cannot continue further, we have to display the choices. First, we need to know if there are any:

	the current number of ink choices

If this number is 0, it means the player reached the end of the story. Else we can get a list of text representing the choices available:

	let L be the current ink choices;

A choice can be made with the following phrase:

	select ink choice number N;

where N is a number representing the choice taken: 1 for the first choice offered to the player, 2 for the second choice, and so on. When a choice is made, we can begin again by showing the next lines.

How the choices are displayed and how the player selects an option is left to the author. We could use, for instance, an HTML list and hyperlinks.

If we want to use the ink story when play begins, we have first to wait until the story JSON has been loaded. This extension creates a text that varies, the "ink initialisation command", for this. If this variable is specified, the game will run this command as soon as the story JSON has been loaded. We can then create an action to handle what happens next.

	The ink initialisation command is "start ink".
	
	Starting ink is an action out of world applying to nothing.
	Understand "start ink" as starting ink.
	
	Carry out starting ink:
		...

The example "The Intercept" shows how a simple ink game could be displayed with Inform.

Part: Advanced usage

Chapter: Tags

Tags in ink are data that are not used directly by the story. They are used to pass informations to Inform, for example the name of a picture to display with Vorple.

The following phrase returns a list of texts containing the tags of the last displayed line:

	current ink tags

It is also possible to get the tags of a knot:

	ink tags of "knot_name"

Finally, we can get the global tags of the ink story:

	global ink tags

Chapter: Jumping to a knot

We can go directly to another part of the ink story with:

	jump to "knot_name" in ink;
	jump to "knot_name.stitch_name";

where the second one makes the story jump to a stitch inside the knot.

Chapter: Manipulating ink variables

We can set an ink variable with:

	set the ink variable "name_of_variable" to X;

where "X" can be a text, a number or a truth state.

We can also get the current value of an ink variable:

	value stored in the ink variable "name_of_variable"
	text stored in the ink variable "name_of_variable"
	number stored in the ink variable "name_of_variable"
	if the ink variable "name_of_variable" is true:

We should use the appropriate phrase depending on the type of the value of the variable.

Chapter: Visit counts

The following phrases return the number of times a knot or a stitch has been visited:

	ink visit count of "knot_name"
	ink visit count of "knot_name.stitch_name"

Chapter: Evaluating ink functions

We can execute an ink function directly from Inform and retrieve its result. For this, we must provide the name of the function and a list of texts containing its arguments.

	evaluate the ink function "function_name" with {"arg1", "arg2"};
	value returned by the ink function "function_name" with {"arg1", "arg2"}
	text returned by the ink function "function_name" with {"arg1", "arg2"}
	number returned by the ink function "function_name" with {"arg1", "arg2"}
	if the ink function "function_name" with {"arg1", "arg2"} returned true:

The list of argument has to be a list of texts, even if the values are numbers or booleans. So {"1", "2"} and not {1, 2}.

 If we want to get the text outputted by an ink function, we can use

	output of the ink function "function_name" with {"arg1", "arg2"}

Chapter: Saving and loading the ink story state

The state of the ink story (containing among others the ink variables) is independant from saved games created by Inform. Even though this extension saves the ink story when the player saves the game and loads it back when the player restores it, we can also get the ink story text whit the phrase

	the ink story state

which returns a text. We can then store it in a text that varies. When we want to load it back, we use

	load the ink story state X;

where "X" is a text.

Note that saving and loading hasn't been tested, so it may or may not work as expected. Contact the author of this extension if you encounter a problem.

Chapter: Advanced: external functions

It is possible to have a function in ink use JavaScript. If a function has been declared in ink like that:

	EXTERNAL multiply(x, y)

The you can bind a JavaScript function to it.

	execute JavaScript command "story.BindExternalFunction('multiply', function(x, y) {return x * y;})";

This should be done as soon as possible, for example in the action resulting from the ink initialisation command.

Chapter: Changelog

Version 2/181103:

	Updated extension for Vorple 3.1.

Version 1/170808:

	Initial version.

Example: *** The Intercept - An Inform 7 port of the game from inkle.

This is a fairly straighforward port of The Intercept, a game written by inkle. The original version made with Unity is available at

	https://github.com/inkle/the-intercept

The "story.json" file and an annotated source is available at

	https://bitbucket.org/Natrium729/the-intercept-in-inform-7/

As it is quite identical to its Unity counterpart, this example doesn't really show the potential of an Inform 7 project using ink; it could have as well been played directly with inkjs. However, it shows how we can present an ink story and its choices.

	*:"The Intercept" by inkle

	Include Vorple Screen Effects by Juhana Leinonen.
	Include Vorple Command Prompt Control by Juhana Leinonen.
	Include Vorple Hyperlinks by Juhana Leinonen.
	Include Vorple Element Manipulation by Juhana Leinonen.
	Include Vorple ink by Nathanael Marion.
	
	Release along with the "Vorple" interpreter.
	Release along with JavaScript "ink.js".
	Release along with the file "story.json".
	
	First when play begins:
		if Vorple is not supported:
			say "Sorry, this story needs the Vorple interpreter.";
			wait for any key;
			stop the game abruptly.

	There is a room.
	
	The initial room description rule is not listed in the startup rulebook.
	
	[TODO: Doesn't work.]
	To decide what text is the sanitised form of (T - a text):
		replace the text "<i>" in T with "[italic type]";
		replace the text "</i>" in T with "[roman type]";
		decide on T.

	When play begins:
		hide the prompt.

	The ink initialisation command is "play ink".
	
	Playing ink is an action out of world applying to nothing.
	Understand "play ink" as playing ink.

	Carry out playing ink:
		scroll to the end of the page;
		display the ink story;
		if the current number of ink choices is greater than 0:
			display the ink choices;
		else:
			end the story.

	To display the ink story:
		while ink can continue:
			say "[sanitised form of the next ink line]";
			scroll to the end of the page;
			wait for any key.

	To display the ink choices:
		open HTML tag "ol" called "choices";
		let L be the current ink choices;
		repeat with X running from 1 to the number of entries in L:
			open HTML tag "li";
			let C be entry X of L;
			now C is the sanitised form of C;
			place a link to command "choose [X]" reading "[C]", without showing the command;
			close HTML tag; [/li]
		close HTML tag; [/ol]
		scroll to the end of the page.

	Choosing is an action out of world applying to a number.
	Understand "choose [number]" as choosing.
	
	Carry out choosing:
		remove the element called "choices";
		select ink choice number (number understood);
		queue the parser command "play ink", without showing the command;