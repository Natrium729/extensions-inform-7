Version 2/181130 of Vorplectron (for Glulx only) by Nathanael Marion begins here.

"Makes it possible for a Vorple story package with Vorplectron to communicate with the latter."

Include Version 3 of Vorple by Juhana Leinonen.

Part - To know if we are in Vorplectron

Chapter - The internal variable - unindexed

In-vorplectron is initially false.

Chapter - To know if we are in Vorplectron

This is the detect if running in Vorplectron rule:
	if Vorple is supported:
		execute JavaScript command "return (typeof vorplectron !== 'undefined')";
		if the Javascript command returned true:
			now in-vorplectron is true.

The detect if running in Vorplectron rule is listed after the detect interpreter's Vorple support rule in the startup rulebook.
the detect if running in Vorplectron rule is listed before the Vorple interface setup stage rule in the startup rulebook.


To decide whether in Vorplectron:
	decide on whether or not in-vorplectron is true.

Part - Sending messages to Vorplectron

To send the message (msg - a text) to Vorplectron:
	if in Vorplectron:
		execute JavaScript command "return vorplectron.send('[escaped msg]')".

To send the message (msg - a text) with argument (arg - a text) to Vorplectron:
	if in Vorplectron:
		execute JavaScript command "return vorplectron.send('[escaped msg]', [arg])".

Chapter - Telling Vorplectron the story is ready

To tell Vorplectron the story is ready:
	send the message "vorpleReady" to Vorplectron.

Chapter - Getting the version of the application

To decide what text is the Vorplectron version:
	if in Vorplectron:
		send the message "getAppVersion" to Vorplectron;
		decide on the text returned by the JavaScript command;
	decide on "".

Chapter - Quitting the Vorplectron app

To quit the/-- Vorplectron application/app:
	send the message "quit" to Vorplectron.

This is the Vorplectron quit the game rule:
	say "[text of the quit the game rule response (A)]";
	if the player consents:
		quit the Vorplectron app.

The Vorplectron quit the game rule substitutes for the quit the game rule when in Vorplectron.

Chapter - Toggling fullscreen

To decide whether Vorplectron is fullscreen:
	if in Vorplectron:
		send the message "isFullScreen" to Vorplectron;
		decide on whether or not the JavaScript command returned true;
	decide on false.

To toggle fullscreen:
	send the message "toggleFullScreen" to Vorplectron.

To enable fullscreen:
	send the message "setFullScreen" with argument "true" to Vorplectron.

To disable fullscreen:
	send the message "setFullScreen" with argument "false" to Vorplectron.

Chapter - The available file types

A Vorplectron file type is a kind of value.
The Vorplectron file types are glkdata and glksave.

Chapter - Saving an external file

To ask the player to save (F - an external file) as a (T - a Vorplectron file type) file:
	if in Vorplectron:
		send the message "saveExternalFile" with argument "{ data:FS.readFile('/gamedata/[name of F].[T]', {encoding: 'binary'}), type:'[T]' }" to Vorplectron.

To ask the player to save (F - an external file):
	ask the player to save F as a glkdata file.

Chapter - Loading an external file

To ask the player to load (F - an external file) as a (T - a Vorplectron file type) file:
	if in Vorplectron:
		execute JavaScript command "
		{
			let data = vorplectron.send('loadExternalFile', '[T]');
			if (data) {
				FS.writeFile('/gamedata/[name of F].[T]', data);
				return true;
			} else {
				return false;
			}
		}
		".

To ask the player to load (F - an external file):
	ask the player to load F as a glkdata file.

Chapter - To expose the filename of an external file (for use without Vorple File Control by Nathanael Marion)

Include (-
[ FileIO_PrintFilename extf;
	if ((extf < 1) || (extf > NO_EXTERNAL_FILES))
		return FileIO_Error(extf, "tried to write to a non-file");
	print (string) TableOfExternalFiles-->extf-->AUXF_FILENAME;
];
-)

To say name of (F - an external file):
	(- FileIO_PrintFilename({F}); -)

Vorplectron ends here.

---- DOCUMENTATION ----

Chapter: Introduction

Section: What is Vorplectron?

Vorplectron allows an author to package a Vorple project into a standalone application for Windows, macOS or Linux. For simple projects, little JavaScript knowledge is required. If you want to customise a Vorplectron application, you'll need some, though.

Instructions and dowloads can be found at the following link:

	https://bitbucket.org/Natrium729/vorplectron

Section: about this extension

This extension allows a Vorple story to communicate with a Vorplectron app, for example to toggle fullscreen. Because of that, this extension is only useful if your story will be packaged into a Vorplectron application.

Chapter: Basic use

Section: Determining whether the story is running in Vorplectron

To know if the story is running inside a Vorplectron application, we can use the following condition:

	if in Vorplectron:

It will return false if Vorple is not supported or if the Vorple story is running inside a normal webpage. It will return true if the story is running inside a Vorplectron application.

All the phrases from this extension already check if in Vorplectron (an do nothing if not), so we generally don't have to do the check ourselves, except if we want the story to execute something else when in Vorplectron.

Section: Telling Vorplectron the story is ready

When the Vorplectron app is launched, it will display a small window telling the story is loading. At the start of the story, we have to tell Vorplectron that the it has finished loading with the following phrase:

	tell Vorplectron the story is ready;

The best place to write it is at the end of the Vorple interface setup rulebook:

	Last Vorple interface setup:
		tell Vorplectron the story is ready.

If we don't tell Vorplectron the story is ready, its window will never show.

Section: Quitting the application

To immediatly quit the Vorplectron application, the following can be used:

	quit the Vorplectron application;

Note that the extension already modifies the behaviour of the quitting the game action so that it quits the application when running in Vorplectron. (The default behaviour of halting the interpreter is kept otherwise.)

Section: Toggling fullscreen

To toggle fullscreen, we can use the following:

	toggle fullscreen;

To make the application fullscreen (and do nothing if it is already fullscreen):

	enable fullscreen;

And for the opposite:

	disable fullscreen;

Finally, to know whether the application is fullscreen:

	if Vorplectron is fullscreen:

The condition will return true if the application is fullscreen, and false if it is windowed or if the story is not running in Vorplectron.

Section: Saving and loading external files

We can prompt the player to save an Inform external file on his computer with the following:

	ask the player to save the File of Glaciers;

A dialog will then appear, asking the player to choose a location and a name for the file.

The phrase does not check if the file exists, so we should not forget to check that ourselves.

We can also prompt the player to load a file from his computer into an Inform external file:

	ask the player to load the File of Glaciers;

A dialog will appear asking the player to choose a file from his computer. The contents of this file will be written in the specified external file. Beware that since the player can choose any file with the "glkdata" extension, he may replace the external file with an unusable one. We can check if the file is usable with the "ready to read" condition, however, so that's not usually a problem.

And again, it's our duty to check if the file exists before using the phrase above.

Additionaly, we can know if the phrases above succeeded or if the player cancelled the process by checking if the JavaScript returned true:

	ask the player to save the file of Glaciers;
	if the JavaScript command returned true:
		say "The file was saved!";
	else:
		save "The saving was cancelled!".

Section: Getting the version of the Vorplectron application

To get a text containing the version of the Vorplectron application as defined in the file "package.json" (see Vorplectron instructions for more information), we can use:

	the Vorplectron version

For example, to display it:

	say "[Vorplectron version]";

It can be useful if you want to display the version of the application in addition to the release number of the story.

Chapter: Advanced use

To use what is explained below, you will have to modifiy the JavaScript files in Vorplectron. Feel free to skip this chapter if you don't need what is explained here.

Section: Sending custom messages

It is possible to send custom messages with the following:

	send the message "ping" to Vorplectron;
	let answer be the text returned by the JavaScript command;

We should be careful to properly handle it in the Vorplectron app. We could for example add the following to the file "custom-listeners.js":

	customListeners.ping = function() {
		return "pong"
	}

With that, answer will have the value "pong".

We can also pass an argument with:

	send the message "pong" with argument "'hello'" to Vorplectron;

The argument is a text representing a JavaScript object. Here it is a string, but we can use anything else, like "{a: 1, b: true, c: 'hello'}" for example.

You have to return a value in Voplectron else the Vorplectron will crash! (If you don't need a returned value, just return null.)

See Vorplectron's instructions for more information.

Chapter: Addititional information

Section: Planned features

I may add the ability to save an external file to Electron "userData" folder.

Section: Known issues

Vorplectron does not handle saving the game yet, so Vorple's default save dialog will show. It is quite buggy however, so I don't recommand using it. I'm waiting for the bug to be fixed before I see how to handle saving in Vorplectron.

Section: Reporting an issue

If you found an issue are would like to make a feature request, please use the bug tracker:

	https://bitbucket.org/Natrium729/vorplectron/issues?status=new&status=open.

You can also contact me at:

	natrium729@gmail.com

Section: Changelog

Version 2/181103:

	Updated the extension for Vorple 3.1.

Version 1/181009:

	Initial release.