Version 1/160527 of Real Time Passing by Nathanael Marion begins here.

"Makes the game time advance in real time. Requires Glulx Real Time by Erik Temple and Flexible Windows by Jon Ingold."

Volume 1 - Dependencies

Book 1 - Glulx Real Time

Include Glulx Real Time by Erik Temple.

Book 2 - Flexible Windows

Include Flexible Windows by Jon Ingold.

Volume 2 - Real time

Book 1 - Necessary windows

Part 1 - Location window

The location window is a text buffer g-window.
The position of the location window is usually g-placeabove.
The scale method of the location window is usually g-fixed-size.
The measurement of the location window is usually 10.

The main window spawns the location window.

Part 2 - Real time output window

The real time output window is a text buffer g-window.
The position is usually g-placeright.
The scale method is usually g-proportional.
The measurement is usually 35.

The main window spawns the real time output window.

Part 3 - Open and actualise the windows

[When play begins (this is the open windows at startup rule):
	open the location window;
	open the real time output window.]

Last carry out going (this is the refresh the windows after going rule):
	if the location window is g-present:
		refresh the location window;
	if the real time output window is g-present:
		clear the real time output window;

Book 2 - Rules for the windows

Part 1 - Rules for the location window

For refreshing the location window:
	clear the location window;
	try looking.

Part 2 - Rules for the real time output window

Chapter 1 - Variables used in advancing the time

[Le facteur de temps détermine la vitesse à laquelle le temps de jeu s'écoule.
Par exemple, avec un facteur de 5, le temps du jeu avancera d'une minute toutes les 5 secondes.]
The time factor is a number that varies. The time factor is usually 5.

[Permet de savoir quand à quel moment il faut incrémenter les minutes de jeu.]
The real time progress is a number that varies. The real time progress is 0.

Chapter 2 - Refreshing the real time output window

This is the minute sequence stage rule:
	if wait interrupted is true:
		now wait interrupted is false;
	if the real time output window is g-present, focus the real time output window;
	follow the minute sequence rules;
	if we are waiting more and wait interrupted is false, clear the real time output window;
	if the real time output window is g-present, focus the main window;
	if the location window is g-present, refresh the location window;
	update the status line;

[When play begins (this is the initialise the game time timer rule):
	every 1 second follow the real time update rule;]

This is the real time update rule:
	increment the real time progress;
	if the real time progress is the time factor:
		now the real time progress is 0;
		follow the minute sequence stage rule.

Chapter 3 - The minute sequence rules

Display timestamp is initially false.

The minute sequence rules is a rulebook.
Every minute rules is a rulebook.

A first minute sequence rule (this is the increment the time of day rule):
	increase the time of day by 1 minute;
	if display timestamp is true, say "[note style][time of day][roman type]";
	[vérifier la date.]

A last minute sequence rule: follow the scene changing rules.
A last minute sequence rule (this is the every minute stage rule): follow the every minute rules.
The timed events rule is listed last in the minute sequence rules.
The update chronological records rule is listed last in the minute sequence rules.
The adjust light rule is listed last in the minute sequence rules.

Chapter 4 - Turn sequence rule modification

The timed events rule is not listed in the turn sequence rulebook.
The increment the turn count rule is listed instead of the advance time rule in the turn sequence rulebook.

This is the increment the turn count rule:
	increment the turn count.

[The declare everything unmentioned rule does nothing when the current focus window is the main window.
[The room description heading rule does nothing when the current focus window is the main window.]
The room description body text rule does nothing when the current focus window is the main window.
The room description paragraphs about objects rule does nothing when the current focus window is the main window.
The check new arrival rule does nothing when the current focus window is the main window.]

Book 3 - Actions controling the flow of time

Part 1 - Pausing the game

Game paused is a truth state that varies. Game paused is false.

Pausing the game is an action out of world applying to nothing.
Understand "pause" as pausing the game.

Carry out pausing the game (this is the pause the game rule):
	[pause virtualized timers;]
	say "The game is now paused and time will not pass. Press the space key to resume." (A);
	wait for the SPACE key while deferring virtual timers;
	say "The game is now resumed." (B).

[Check pausing the game (this is the can't pause an already paused game rule):
	if game paused is true, say "The game is already paused!" (A) instead.

Carry out pausing the game (this is the pause the game rule):
	now game paused is true;
	pause virtualized timers.

Report pausing the game (this is the report pausing the game rule):
	say "The game is now paused. Time will not pass until you resume the game." (A).

[Before doing something when game paused is true (this is the can't play when game is paused rule):
	say "Le jeu est en pause. Tapez « reprendre » pour recommencer à jouer." (A);
	stop the action.]

First after reading a command when game paused is true (this is the can't play during pause rule):
	unless the player's command includes "resume" or the player's command includes "reprendre":
		say "The game is paused. Type 'resume' to continue playing." (A);
		reject the player's command.

Part 2 - Reprendre le jeu

Resuming the game is an action out of world applying to nothing.
Understand "resume" as resuming the game.

Check resuming the game (this is the can't resume an arlready unpaused game rule):
	if game paused is false, say "The game is not paused!" (A) instead.

Carry out resuming the game (this is the resume the game rule):
	now game paused is false;
	restart virtualized timers.

Report resuming the game (this is the standard report resuming the game rule):
	say "The game is resumed." (A).]

Part 3 - Altering the flow of time

Altering the flow of time is an action out of world applying to a number.
Understand "time factor [number]" as altering the flow of time.

Check altering the flow of time (this is the can't set a time factor out of range rule):
	if number understood < 1 or number understood > 3600, say "The time factor has to be between 1 and 3600, inclusive." (A) instead.

Carry out altering the flow of time (this is the modify the time factor rule):
	now the time factor is the number understood;
	if the real time progress is greater than the time factor, now the real time progress is the time factor - 1.

Report altering the flow of time (this is the standard report altering the flow of time rule):
	say "From now, [number understood] real second[s] will be needed to increase the game time by 1 minute." (A).

Volume 3 - Le temps du jeu

[
Book 1 - Unités fondamentales du temps

Le jour actuel est un nombre qui varie. Le jour actuel est 1.

Part 1 - Heures et minutes

[Ce qui suit permet de tester s'il fait nuit ou jour (ex. : if it is night, blabla), le changement jour/nuit se fait donc automatiquement.]
To decide whether il fait jour:
	let H be the hours part of the time of day;
	if H >= 6 and H < 21, yes;
	no.

To decide whether il fait nuit:
	if il fait jour, no;
	yes.

Part 2 - Jours

Un jour-semaine is a kind of value. Les jour-semaine sont lundi, mardi, mercredi, jeudi, vendredi, samedi et dimanche.

Le jour-semaine actuel est un jour-semaine qui varie.

[Permet de tester si on est dans une nuit de tel jour à tel jour. Remplace les truth state "NuitdeXàY".]
To decide whether dans la nuit de (D - un jour-semaine): [Pour la nuit de lundi à mardi, on utilise lundi comme jour.]
	si le jour-semaine actuel est D et l' heure actuelle >= 9:00 PM, oui;
	si (le jour-semaine actuel est le jour-semaine après D) et l' heure actuelle < 6:00 AM, oui;
	non.

[
[permet d'afficher "nuit de x à y" si on est la nuit, et juste le nom de la journée si on est le jour.]
To say current weekday with nights:
	if it is day:
		say "[current weekday]";
	else:
		say "nuit de ";
		if the current time > 21:00:00, say "[current weekday] à [weekday after current weekday]";
		 else say "[weekday before current weekday] à [current weekday]".
]

Part 3 - Mois

Un mois is a kind of value. Les mois sont janvier, février, mars, avril, mai, juin, juillet, août, septembre, octobre, novembre et décembre.

Un mois has a number called longueur. La longueur de janvier est 31. La longueur de février est 28. La longueur de mars est 31. La longueur de avril est 30. La longueur de mai est 31. La longueur de juin est 30. La longueur de juillet est 31. La longueur de août est 31. La longueur de septembre est 30. La longueur de octobre est 31. La longueur de novembre est 30. La longueur de décembre est 31.

Le mois actuel est un mois qui varie.
Le jour du mois actuel est un nombre qui varie. Le jour du mois actuel est 1.

Part 4 - Années

L' année actuelle est un nombre qui varie. L' année actuelle est 2015.
[The current yearday is a number variable. The current yearday is 1.]

Definition: a number (called year) is bissextile:
	if the remainder after dividing the year by 4 is not 0, no;
	if the remainder after dividing the year by 100 is not 0, yes;
	if the remainder after dividing the year by 400 is not 0, no;
	yes.

Part 5 - Dire la date

[Permet de dire la date au complet.]
To say date actuelle:
	say "[jour-semaine actuel] [jour du mois actuel][if the jour du mois actuel is 1]er[end if] [mois actuel] [année actuelle]".
]

Book 1 - Waiting more

[Advancing the clock is a truth state that varies. Advancing the clock is false.]
Wait interrupted is a truth state that varies. Wait interrupted is false.

To interrupt the/-- wait:
	now wait interrupted is true.

[Every turn when wait interrupted is true:
	now wait interrupted is false.]

Waiting more is an action applying to one number.
Understand "wait [a time period]" or "wait for [a time period]" or "wait for a/an [a time period]" or "wait a/an [a time period]" as waiting more.

[On empêche le joueur d'attendre plus d'une journée, car le code n'a pas été conçu pour passer plus d'une journée à la fois.]
Check waiting more (this is the can't wait too long rule):
	if the time understood > 23 hours, say "[We] [do not have] that kind of patience." (A) instead.

[On fait avancer le temps.]
Carry out waiting more (this is the standard carry out waiting more rule):
	pause virtualized timers;
	[now advancing the clock is true;]
	let the target time be the time of day plus the time understood;
	decrease the target time by one minute;
	while the time of day is not the target time:
		follow the minute sequence stage rule;
		if wait interrupted is true:
			now wait interrupted is false;
			break;
		[follow the minute sequence rules;]
		[vérifier la date;] 
	[now advancing the clock is false;]
	if the location window is g-present, refresh the location window;
	restart virtualized timers.

Report waiting more (this is the standard report waiting more rule):
	say "[We] [wait] for a moment. [It] [is] [now] [time of day]." (A);
	[try looking.]

Part 1 - Vérifier la date

[Le code qui permet de vérifier si on est passé à une nouvelle journée.]
[
To vérifier la date:
	if l' heure actuelle est 12:00 AM: [c-à-d si on est passé à une nouvelle journée]
		maintenant le jour-semaine actuel est le jour-semaine après le jour-semaine actuel; [On change le jour pour le jour suivant]
		incrémenter le jour du mois actuel;
		[incrémenter le current yearday;]
		incrémenter le jour actuel;
		si l' année actuelle est bissextile et le mois actuel est février et le jour du mois actuel > 29: [on teste d'abord l'exception des années bissextiles]
			maintenant le jour du mois actuel est 1;
			maintenant le mois actuel est mars;
		sinon si le jour du mois actuel > la longueur du mois actuel: [le changement du mois.]
			maintenant le jour du mois actuel est 1;
			maintenant le mois actuel est le mois après le mois actuel;
			si le mois actuel est janvier: [on commence une nouvelle année.]
				incrémenter l' année actuelle.
]

Volume 4 - French Translation (for use with French Language by Nathanael Marion)

The location window translates into French as la fenêtre de l'emplacement.
The real time output window translates into French as la fenêtre de sortie en temps réel.

The can't wait too long rule response (A) is "[Tu] [n'as pas] la patience d'attendre aussi longtemps.".
The standard report waiting more rule response (A) is "[Tu-j'][attends] un moment. Il est maintenant [time of day].".
The pause the game rule response (A) is "Le jeu est maintenant en pause et le temps ne s'écoulera plus. Appuyez sur la barre d'espace pour reprendre.".
The pause the game rule response (B) is "Le jeu n'est plus en pause.".
[The can't pause an already paused game rule response (A) is "Le jeu est déjà en pause !".
The report pausing the game rule response (A) is "Le jeu est maintenant en pause, et le temps ne s'écoulera plus jusqu'à ce que vous le reprenez.".
The can't play during pause rule response (A) is "Le jeu est en pause. Tapez « reprendre » pour recommencer à jouer.".
The can't resume an arlready unpaused game rule response (A) is "Le jeu n'est pas en pause !".
The standard report resuming the game rule response (A) is "Le jeu n'est plus en pause.".]
The standard report altering the flow of time rule response (A) is "À partir de maintenant, [number understood] seconde[s] réelle[s] correspondr[if number understood is 1]a[else]ont[end if] à 1 minute de jeu.".
The standard report altering the flow of time rule response (A) is "À partir de maintenant, [number understood] seconde[s] réelle[s] correspondr[if number understood is 1]a[else]ont[end if] à 1 minute de jeu.".
The can't set a time factor out of range rule response (A) is "Le facteur de temps doit être compris entre 1 et 3600, inclusivement.".

[Understand "reprendre" as resuming the game.]
Understand "facteur de/-- temps [number]" as altering the flow of time.
Understand "attendre [a time period]" as waiting more.

Real Time Passing ends here.

---- DOCUMENTATION ----

The documentation will soon be written.
