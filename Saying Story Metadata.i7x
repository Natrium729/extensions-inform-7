Version 1/201028 of Saying Story Metadata by Nathanael Marion begins here.

"Adds phrases to say story metadata such as the serial number or the Inform 6 compiler version."

Use authorial modesty.

Part - The serial number

To say serial number:
	(- PrintSerialNumber(); -)

Chapter - Z-machine serial number (for Z-machine only)

Include (-
[ PrintSerialNumber i;
	for (i=0 : i<6 : i++) print (char) HDR_GAMESERIAL->i;
];
-)

Chapter - Glulx serial number (for Glulx only)

Include (-
[ PrintSerialNumber i;
	for (i=0 : i<6 : i++) print (char) ROM_GAMESERIAL->i;
];
-)

Part - The Inform 7 build number

To say I7 build number:
	(- print (PrintI6Text) NI_BUILD_COUNT; -)

Part - The Inform 6 compiler version

To say I6 compiler version:
	(- inversion; -)

Part - The Inform 6 lib version

To say I6 lib version:
	(- print (PrintI6Text) LibRelease; -)

Part - The IFID

To say IFID:
	(- PrintIfid(); -)

Include (-
[ PrintIfid i;
	for (i=6: i <= UUID_ARRAY->0: i++) print (char) UUID_ARRAY->i;
];
-)

Part - The interpreter version

To say interpreter version:
	(- PrintTerpVersion(); -)

Chapter - Z-machine interpreter version (for Z-machine only)

[On Z-machine, the version of the standard that the interpreter implements is printed (or the terp number if it doesn't implement a standard), as well as the interpreter version.

Example: "1.1 (6F)"]

Include (-
[ PrintTerpVersion;
	if (standard_interpreter > 0) {
		print standard_interpreter/256, ".", standard_interpreter%256, " (", HDR_TERPNUMBER->0;
		#Iftrue (#version_number == 6);
		print (char) '.', HDR_TERPVERSION->0;
		#Ifnot;
		print (char) HDR_TERPVERSION->0;
		#Endif;
		print ")";
	} else {
		print HDR_TERPNUMBER->0, " Version ";
		#Iftrue (#version_number == 6);
		print HDR_TERPVERSION->0;
		#Ifnot;
		print (char) HDR_TERPVERSION->0;
		#Endif;
	}
];
-)

Chapter - Glulx interpreter version (for Glulx only)

[On Glulx, the version of the interpreter as reported by the interpreter is printed.]

Include (-
[ PrintTerpVersion i;
	@gestalt 1 0 i;
	print i / $10000, ".", (i & $FF00) / $100, ".", i & $FF;
];
-)

Part - The library serial number

To say library serial number:
	(- print (string) LibSerial; -)

Part - The VM version (for Glulx only)

To say VM version:
	(- PrintVmVersion(); -)

Include (-
[ PrintVmVersion i;
	@gestalt 0 0 i;
	print i / $10000, ".", (i & $FF00) / $100, ".", i & $FF;
];
-)

Saying Story Metadata ends here.

---- DOCUMENTATION ----

This extension makes it possible to say diverse story metadata usually printed in the banner text, such as the serial number or Inform 6 version used.

Chapter: Phrases common to both Glulx and the Z-machine

	say "[serial number]";

Shows the serial number, which is the date the story was compiled, in the format "AAMMDD".

	say "[I7 build number]";

Shows the build on Inform 7 used to compile the story, for example "6L38".

	say "[I6 compiler version]";

Shows the version of Inform 6 used to compile the story, for example "6.33".

	say "[I6 lib version]";

Shows the version of the Inform 6 library used, for example "6/12N".

	say "[IFID]";

Shows the IFID of the story.

	say "[library serial number]"

Shows the serial number of the Inform 6 library used.

Chapter: Phrases available on Glulx

	say "[interpreter version]";

Shows the version of the interpreter used to play the story, for example "1.3.3". (Each interpreter will show something different.)

	say "[VM version]";

Shows the version of the Glulx specification implemented by the interpreter used to play the story; for example "3.1.2".

Chapter: Phrases available on the Z-machine

	say "[interpreter version]";

Shows the version of Z-machine specification the interpreter used to play the story, as well as the interpreter number; for example "1.1 (6F)".
