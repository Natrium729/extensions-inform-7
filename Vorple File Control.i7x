Version 1/180513 of Vorple File Control by Nathanael Marion begins here.

"Allows the player to download or upload external files. Requires version 3 of Vorple."

Part 1 - Requirements

Include version 3 of Vorple by Juhana Leinonen.

Part 2 - To expose some properties of external files

Include (-
[ FileIO_PrintFilename extf;
	if ((extf < 1) || (extf > NO_EXTERNAL_FILES))
		return FileIO_Error(extf, "tried to write to a non-file");
	print (string) TableOfExternalFiles-->extf-->AUXF_FILENAME;
];

[ FileIO_IsBinary extf;
	if ((extf < 1) || (extf > NO_EXTERNAL_FILES))
		return FileIO_Error(extf, "tried to write to a non-file");
	return TableOfExternalFiles-->extf-->AUXF_BINARY;
];
-)

To say name of (FN - an external file):
	(- FileIO_PrintFilename({FN}); -)

Definition: an external file is binary if I6 routine "FileIO_IsBinary" says so (it is declared as a binary file instead of a normal plain text file).

Part 3 - To download a file

To download (F - an external file) as (name - a text) with type (T - a text), without header:
	if ready to read F:
		let JS strip header be "";
		if without header and F is not binary:
			let JS strip header be "content = content.substr(content.indexOf('\n') + 1);";
		execute JavaScript command "
			var content = FS.readFile('/gamedata/[name of F].glkdata', { encoding: '[if F is binary]binary[else]utf8[end if]' });
			[JS strip header]
			var url = URL.createObjectURL(new Blob([bracket]content[close bracket], {type: '[T]'}));
			$('body').append('<a id=\'externalfiledownload\' style=\'display: none;\' href=\'' + url + '\' download=\'' + '[name]' + '\'></a>');
			var downloadlink = $('#externalfiledownload');
			downloadlink[bracket]0[close bracket].click();
			downloadlink.remove();
			URL.revokeObjectURL(url);".

To download (F - an external file) as (name - a text), without header:
	if without header:
		download F as name with type "[if F is binary]application/octet-stream[else]text/plain[end if]", without header;
	else:
		download F as name with type "[if F is binary]application/octet-stream[else]text/plain[end if]".
	

To download (F - an external file), without header:
	if without header:
		download F as "[name of F].glkdata" with type "[if F is binary]text/plain[else]application/octet-stream[end if]", without header;
	else:
		download F as "[name of F].glkdata" with type "[if F is binary]text/plain[else]application/octet-stream[end if]".
		
Part 4 - To upload a file

Waiting-for-upload is initially "[bracket]If the dialog to upload a file does not open, please click on the folowing button.[close bracket]".
File-uploaded is initially "[bracket]The file has been successfully uploaded![close bracket]".
File-not-uploaded is initially "[bracket]No file has been uploaded.[close bracket]".

Upload successful is initially true.
The uploaded file is an external file that varies.

To ask to upload a/-- file of type (T - a text) to (F - an external file):
	now upload successful is false;
	now the uploaded file is F;
	place an inline element called "externalfileupload-legend" reading "[waiting-for-upload][line break]";
	execute JavaScript command "
		$('.externalfileupload').remove();
		var loadfile = function(event) {
			var input = event.target;
			var reader = new FileReader();
			reader.onload = function() {
				var data = reader.result;
				FS.writeFile('/gamedata/[name of F].glkdata', data);
				$('.externalfileupload-legend').last().html('[escaped file-uploaded]');
				$('.externalfileupload').remove();
				vorple.prompt.queueCommand('__uploadsuccessful', true);
			};
			reader.[if F is binary]readAsArrayBuffer[else]readAsText[end if](input.files[bracket]0[close bracket]);
		};
		vorple.layout.openTag('input', 'externalfileupload');
		vorple.layout.closeTag();
		var uploadfield = $('.externalfileupload').last();
		uploadfield.attr({
			id: ''externalfileupload',
			type: 'file',
			[if T is not empty]accept: '[escaped T]',[end if]
			onchange: 'loadfile(event)'
		});
		uploadfield.click();";

Handling upload is an activity.

First after reading a command when the player's command matches "__uploadsuccessful":
	now upload successful is true;
	carry out the handling upload activity.

After reading a command when upload successful is false:
	now upload successful is true;
	execute JavaScript command "
		$('.externalfileupload-legend').last().html('[escaped file-not-uploaded]');
		$('.externalfileupload').remove();";

To ask to upload a/-- file to (F - an external file):
	ask to upload a file of type "" to F.

Vorple File Control ends here.